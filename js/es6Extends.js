class Dialog {
  constructor(container){
    this.container = document.querySelector(container)
  }
  hide(){
    this.container.style.display = 'none';
  }
  show(){
    this.container.style.display = 'block';
  }
}


class DragDialog extends Dialog {
  constructor(){
    super()
    // 调用完super才能使用this
  }
}