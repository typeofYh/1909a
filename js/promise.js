const games = [
  {
    key:1,
    res:'第一关钥匙'
  },
  {
    key:2,
    res:'第二关钥匙'
  },
  {
    key:3,
    res:'第三关钥匙'
  }
] 

const onceGame = (key, ck) => {  // 每一关游戏
  setTimeout(() => { // 浏览器调用的
    // 成功
    // 得到res
    const res = games.find(item => item.key === key);
    ck(res);
  }, Math.floor(Math.random() * 10) * 1000)
}
// 回调函数套回调函数 嵌套层数过多的时候会形成回调地狱
// onceGame(1, function(res){
//   console.log(1, res);
//   onceGame(2, function(res){
//     console.log(2, res)
//     onceGame(3, function(res){
//       console.log(3, res)
//     })
//   })
// });  // 第一关 怎么能在这拿到第一关的钥匙，开始第二关


const oncePromiseGame = (key) => {
  return new Promise((resolve, reject) => { // 同步操作 立即执行
    console.log('oncePromiseGame', key)
    onceGame(key, resolve)
  })
}

// oncePromiseGame(1)
// .then((res) => {
//   console.log(1, res);
//   return oncePromiseGame(2)
// }, (error) => {
//   console.log(error);
// })
// .then(res => {
//   console.log(2, res);
//   return oncePromiseGame(3)
// })
// .then(res => {
//   console.log(3, res);
// })
// promise 其实就是一个包裹异步操作的容器
// 把回调函数形式改成链式调用形式了
// 它有三种状态 pedding(加载中) fufilled(成功) reject（失败）
// 只能从pedding -> fufilled  或者 pedding -> reject

async function run (){
  const res = await oncePromiseGame(1)
  console.log(res);
  const res1 = await oncePromiseGame(2)
  console.log(res1);
  const res2 = await oncePromiseGame(3)
  console.log(res2);
}
// const res = run();
// res.then(o => {
//   console.log('then-----',o);
// }).catch(erro => {
//   console.log('error---',erro);
// })
// async函数跟普通函数调用方式一样，返回值是promise对象
// async函数在调用时会自动执行函数内脚本，碰到异常退出函数执行，promise变成失败状态，顺利执行完成，promise变成成功状态得到函数返回值
// async是generator函数的语法糖，作用控制异步脚本执行顺序的，让异步任务按顺序执行
// 把await看成一个节点

const run1 = function*(){
  console.log('1111');
  const r = yield oncePromiseGame(1);
  console.log('yiele r----', r);
  const res1 = yield oncePromiseGame(2)
  console.log(res1);
  const res2 = yield oncePromiseGame(3)
  console.log(res2);
}
// const o = run1()
// o.next().value.then(res => {
//   console.log(res);
//   o.next(res);
// })

// 当前遍历指针所在的那个位置的信息对象 {done:boolean true代表遍历完成false遍历未完成, value:值}
// console.log(o.next());
// console.log(o.next());
// generator函数调用方式跟普通函数一样
// 需要手动控制函数内部脚本执行,可以控制函数内部状态
// 返回值是遍历器对象（Iterator Object）, 遍历器对象有一个方法next


// 1. 当我们同时执行多个异步操作的时候，使用promise.all,接受一个数组，返回一个promise对象
// Promise是个函数，Promise的静态方法

// 静态方法和实例方法有什么区别
// this指向不同，静态方法的this指向构造函数，实例方法的this指向实例对象
// 静态方法定义在函数对象上 实例和原型方法定义在实例对象上

Promise.myAll = function(PromiseQueue){
  if(!Array.isArray(PromiseQueue)){
    throw new Error('object is not iterable (cannot read property Symbol(Symbol.iterator))')
  }
  return new Promise((resolve, reject) => {
    let myAllResult = [];
    let count = 0;
    PromiseQueue.map((ItemPromise, i) => { // 同步操作
      ItemPromise.then(res => { // 异步操作
        myAllResult[i] = res;
        count++;
        // console.log(myAllResult); 
        if(count === PromiseQueue.length){
          resolve(myAllResult);
        }
      }).catch(error => {
        reject(error);
      })
    })
  })
}


// race 和 all方法的区别

const getNum = (time) => new Promise((resolve) => {
  setTimeout(() => {
    resolve(time);
  }, time)
});


let startTiem = Date.now();
console.log(`start Time: ${startTiem}`)

Promise.myAll([getNum(100), getNum(150), getNum(50)]).then(res => {
  console.log('Promise all times:' + (Date.now() - startTiem));
  console.log(res);
})

async function run(){
  let res = []
  res[0] = await getNum(100)
  res[1] =await getNum(150)
  res[2] =await getNum(50)
  console.log('run async times:' + (Date.now() - startTiem));
  console.log(res);
}
// run();