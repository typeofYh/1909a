const http = require('http');
const fs = require('fs');
const zlib = require('zlib');
const path = require('path');

const publicPath = (file = '') => path.join(__dirname, 'public', file);
const getEtag = () => Math.random().toString().slice(2) // 应该根据文件内容生成，监听文件变化
let etag = getEtag();
const server = http.createServer((req, res) => {
  console.log('request info: \n url:', req.url + '\n');
  if(req.url === '/'){
    res.end(fs.readFileSync(publicPath('index.html')))
    return;
  }
  // 判断当前请求的是否在public中存在
  if(fs.existsSync(publicPath(req.url))){
    // 压缩
    res.setHeader('content-encoding','gzip')
    // 设置强缓存响应头
    res.setHeader('cache-control', 'max-age=10')
    res.setHeader('expires', new Date('2022-07-16').toGMTString())
    // 强缓存失效，会走协商缓存，判断文件变化，文件发生变化返回新文件，没有变化返回304状态码
    const fileContent = fs.readFileSync(publicPath(req.url));
    const fileInfo = fs.statSync(publicPath(req.url));
    const ifNoneMatch = req.headers['if-none-match'];
    const lastTime = req.headers['if-modified-since']; 
    if(lastTime === fileInfo.mtime.toGMTString()){ 
      res.statusCode = 304;
      res.end(); 
      return;
    }
    if(ifNoneMatch === etag){ // etag命中了文件没有变化返回304状态码不会返回新资源
      res.statusCode = 304;
      res.end(); 
      return;
    }
    res.setHeader('etag', etag);
    res.setHeader('last-modified', fileInfo.mtime.toGMTString()); // 文件最后一次修改时间
    res.statusCode = 200;
    res.end(fileContent) // 缓存没有命中返回新资源
    return;
  }
}).listen(3001, () => {
  console.log('port is 3000')
})