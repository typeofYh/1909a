
### 简历

简洁 清晰 3页内

1. 个人简介

- 姓名 工作年限 

手机号 和 邮箱 不要写错了

2. 技能

基础语言：
  javaScript、es6、es7；promise\async\class\esModule
  typeScript: javaScript的超集
  css\css3; transition\animation\transfrom\flex
  css预编译语言 less（antd样式 通过less进行编写）、sass
  html\html5；canvas\语义化标签\history\localeStorage\sessinStorage
框架类：
  vue2 + vue-router@2.x + vuex@2.x + ui库 (pc elementUi  、移动端 vant)
  vue3 + vue-router@3.x + vuex@3.x + ui库 (pc antd of vue、 移动端 vant)
  react + react hooks@16.8 + react-router-dom + （mobx/redux) 
  umijs 基于redux继承dva  ui库（pc antd、 移动端 antdMobile）
  中后台系统 umijs + antd pro  ui库(proComponent)
  uniapp 移动端 (h5 + 小程序) vue语法
方法类：
  swiper、better-scroll、jQuery、lodash、dayjs
网络层面：
  http/https协议、socket协议
  http/https协议通过 ajax 对象/fetch 发送
  axios、axios二次封装
  ahooks useRequest()
  前后端联调
  socketIo/ 浏览器自带的WebSocket构造函数
构建工具：
  webpack、gulp、babel
  整体上线流程：
    1. 先打包通过webpack打包生成打包之后的文件（html、css、js），把vue文件打包成js（通过不同的loader处理文件）
    2. 放到服务器（通过FileZilla上传文件），启动nginx服务处理跨域问题
    3. 重启服务器

    自动化部署，通过jenkins自动化部署的

    jenkins工作原理：绑定git远程仓库， 前端只需要把代码提交到仓库，选择提交记录点击立即构建
版本管理工具：
  git
数据可视化：
  echarts、antv、datav

了解、熟悉、掌握、精通 准确描述对该技能点的使用情况

3. 工作经历
时间  公司名称 职位
岗位职责：
  在这个公司你做了什么；


4. 项目经历：
  项目名称 项目运行环境
  技术栈
  在这个项目中做了什么；
  
  cctv pc端
  实现cctv视频首页、直播、频道页面开发
  利用videojs、videojs-contrib-hlsjs实现直播播放器，全屏播放，播放进度条等功能
  封装实现整体网站交互组件（如公共图片组件、tab切换、动画、上拉加载），优化用户交互。
  实现整体数据获取渲染，解决页面兼容问题

  cctv h5

