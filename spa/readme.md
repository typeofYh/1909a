<!--
 * @Author: Yh
 * @LastEditors: Yh
-->

- 在浏览器url地址栏按回车会发生什么？
1. 通过dns解析 解析域名和ip的对应关系
2. 通过ip地址建立tcp链接
3. 发起3次握手
4. 发起http请求，得到响应数据，进行渲染处理
5. 发起4次挥手，断开tcp链接


- 什么是http？
超文本传输协议，主要针对浏览器和服务器进行通信的

- 页面中哪些可以发起http请求
1. 在浏览器url地址按回车发起http请求，get方式，一般返回html资源
2. html元素 link标签href属性 script标签的src属性 img标签的src属性， get方法 ， 返回css资源，图片资源，js资源，属于静态资源问题
3. ajax和fetch(同源策略限制会有跨域问题) 可以主动发起http请求，向服务获取接口数据

ajax是一种技术，实现在不加载整个页面的情况下，获取最新数据


SAP是什么？
single page web application
单页面应用
加载单个HTML 页面并在用户与应用程序交互时动态更新该页面的Web应用程序



通过原生a标签切换的页面会重新加载整个页面资源
但是通过react-router-dom提供的Link标签跳转不会加载整个页面，只会加载变化的js文件
为什么？

路由有两种模式： hash #   history 
1. 切换页面地址，不需要让页面重新加载 

history模式的路由会出现404， 怎么解决404问题


浏览器前进后退怎么实现，hash 和 history

1. 什么是spa，怎么实现spa
2. hash 和 history模式的区别
3. 怎么解决history模式404的问题



