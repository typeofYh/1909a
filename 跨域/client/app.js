const http = require('http');
const fs = require('fs');
const path = require('path');
const server = http.createServer((req, res) => {
  if(req.url === '/'){
    res.end(fs.readFileSync(path.join(__dirname,'./index.html')))
  }
})

server.listen('8000');
