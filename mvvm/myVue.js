class Vue {
  constructor({el, data}){
    this.$data = data;
    this.$el = document.querySelector(el);
    // 劫持数据 监听data内所有数据变化
    this.Observer(data);
    // 解析视图 compile
    this.Compiler(this.$el);
  }
  /**
   * [Compiler 视图解析]
   *
   * @return  {[type]}  [return description]
   */
  Compiler(el){
    const reg = /\{\{([^\}]+)\}\}/g;
    // 指令部分
    const childs = [...el.childNodes];
    if(childs.length){
      childs.forEach(el => {
        // 判断是文本节点还是元素节点
        switch(el.nodeType){
          case 3: // 文本模板部分 {{}}
            if(reg.test(el.nodeValue)){
              // console.log(el.nodeValue);
              // 替换的模板
            }
          break;
          case 1: 
            const attributes = [...el.attributes]; // 获取元素上所有的属性节点
            attributes.forEach((attrEl) => {
              if(attrEl.nodeName.startsWith('v-')){ //属性名以v-开始的 指令部分
                console.log(attrEl, el);
              }
            })
          break;
        }
        this.Compiler(el);
      })
    }
  }
  /**
   * [Observer 劫持数据]
   *
   * @return  {[type]}  [return description]
   */
  Observer(data){
    // 判断data是不是一个对象
    if(!(typeof data === "object")){
      return;
    }
    // 遍历对象 把对象的每一个属性变成访问器属性
    Object.keys(data).forEach(key => {
      // console.log(key);
      let value = data[key];
      this.ObserverKeys(data, key, value);
    })
  }
  // 每一个属性变成访问器属性
  ObserverKeys($data, _key, _value){ // info
    this.Observer(_value); // 递归
    Object.defineProperty($data, _key, {
      get(){
        return _value;
      },
      set(val){
        console.log(val);
      }
    })
  }
}