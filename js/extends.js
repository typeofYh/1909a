function Dialog (container){
  console.log(this);
  this.container = document.querySelector(container);
}
Dialog.prototype.toggle = function(){
  this.container.style.display = this.container.style.display === 'none' ? 'block' : 'none';
}
Dialog.prototype.hide = function(){
  // console.log('原型方法 hide', this);
  this.container.style.display = 'none';
}
Dialog.prototype.show = function(){
  // console.log('原型方法 hide', this);
  this.container.style.display = 'block';
}

// 继承 拖拽 
function DragDialog (container){
  // DragDialog this 实例对象
  // 拿到container
  // 借用构造函数 继承私有属性和方法
  Dialog.call(this, container) // 普通函数调用 Dialog this指向window
}
// 原型继承   借用构造函数 + 原型继承
DragDialog.prototype = Object.create(Dialog.prototype);
DragDialog.constructor = DragDialog;
// 修改子类影响父类
DragDialog.prototype.drag = function(){
  console.log 


const _dialog1 = new DragDialog('#dialog');


// const _dialog = new Dialog('#dialog'); 
// // 通过new关键字调用的函数是构造函数 返回实例对象
// _dialog.hide(); // 弹窗不显示
// _dialog 实例对象 私有方法 去找原型方法

const btn = document.querySelector('#btn');
btn.addEventListener('click', () => {
  _dialog.toggle();
})



// class 其实是面向对象编程的语法糖 简化语法