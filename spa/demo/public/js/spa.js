/*
 * @Author: Yh
 * @LastEditors: Yh
 */
// console.log('spa');

// const allA = [...document.querySelectorAll('a')];
// allA.forEach(item => {
//   item.addEventListener('click', (e) => {
//     e.preventDefault();
//     // console.log()
//     const url = item.getAttribute('href');
//     // hash 模式路由 在地址后添加#号
//     // window.location.hash = "#" + url;

//     // history 模式路由 通过window.history来跳转
//     window.history.pushState(null, '', url);
//   })
// })


class Router {
  constructor(config = {mode:'history',routes:[]}) {
    this.mode = config.mode;
    this.routes = config.routes;
    this.routerView = document.getElementById('router-view');
    // 获取当前页面地址，渲染对应视图
    // console.log(window.location);
    this.renderView(this.mode === 'history' ? window.location.pathname : (window.location.hash ? window.location.hash.slice(1) : '/'));
    // 阻止a标签的默认行为，使用hash或者history来跳转页面
    this.defultLink();
  }
  renderView(url){
    // console.log(url);
    const { component } = this.routes.find(({path}) =>  path === url);
    // console.log(component);
    this.routerView.innerHTML = component;
  }
  defultLink(){
    document.body.addEventListener('click', e => {
      if(e.target.nodeName === 'A'){
        e.preventDefault(); // 阻止a标签的默认行为
        this.jumpUrl(e.target.getAttribute('href'));
      }
    })
  }
  jumpUrl(url){
    // console.log(url, this.mode);
    if(this.mode === 'hash'){
      window.location.hash = '#' + url;
    }

    if(this.mode === 'history'){
      window.history.pushState(null, '', url);
    }

    this.renderView(url);
  }
}
