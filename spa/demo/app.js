/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const http = require('http');
const fs = require('fs');
const path = require('path');
const publicFilePath = (filename) => path.join(__dirname,'public',filename)
const paths = ['/','/my','/list']
const serve = http.createServer((req, res) => { // 只要对3001发起http请求就执行一次
  console.log(`req url : ${req.url}`)
  if(req.url === '/favicon.ico') {
    res.end('');
    return;
  }
  if(paths.includes(req.url)){
    res.end(fs.readFileSync(publicFilePath('index.html')))
    return;
  }
  if(req.url === '/js/spa.js'){
    res.end(fs.readFileSync(publicFilePath(req.url)))
    return;
  }

  res.statusCode = 404;
  res.end('404')
  
})

serve.listen(3001, () => {
  console.log('serve is port 3001')
})
