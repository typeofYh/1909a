## 通过ajax和fetch发起的请求会产生跨域问题！

1. 怎么会产生跨域问题，因为同源策略，要同协议，同域名，同端口

2. 解决跨域

- proxy 反向代理

- 请求当前服务器。然后服务器转发请求到接口服务器，拿到数据，服务器返回到前端
- 开发环境： 
  通过webpack devServer的proxy属性来实现
- 生产环境:
  通过nginx（软件）修改配置（proxy_pass）可以实现

- cors 跨域资源共享 通过服务端配置响应头来解决的

- jsonp 利用script标签src属性，来请求数据，但是script src只支持get方式请求，接口需要单独定义，要定义成回调函数的形式

- postMessage 主窗口和子窗口通信


## 移动端调试
1. 引入`vconsole`在手机中打开移动端控制台
2. 抓包工具（charles、whistle），抓取手机或者其他请求请求