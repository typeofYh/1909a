### 什么是mvvm？

m: model
v: view
vm: viewModel（监听数据变化同时驱动视图更新）


有mvc演变过来的
model 数据
view 视图
control 控制器


vue怎么实现数据驱动视图，首先解析组件内data,返回一个对象，然后通过递归配合Object.defineProperty,
把对象内每一个属性变成访问器属性，然后再getter函数中添加watcher监听者到dep，在setter中比较属性值变化，属性发生变化
通知dep中所有watcher，在watcher中进行compiler视图解析更新视图

1. 组件内data为什么是函数
工厂模式，为了隔离每一个组件的状态互不影响，保持独立的对象地址

2. watch 和 computed
watch是监听属性变化的，只能设置一个监听函数，在函数中得到属性当前值和上一次改变的值
compute用来定义计算属性，得到一个新属性，依赖属性中涉及的属性进行计算，在watch的基础上加了一个lazy缓存

3. watch 怎么深度监听 通过deep属性深度监听

4. 碰到后添加的属性怎么让视图发生变化，通过$set来添加

5. 生命周期
beforeCreate 找不到this，读取data
created 组件实例生成完成，data挂载完成
创建阶段
beforeMount dom元素没有初始化加载完成
mounted dom元素初始加载完成
挂载阶段
beforeUpdate 开始比较属性，触发watcher，通知虚拟dom进行diff比较
updated dom更新完成 会执行 this.$nextTick(() => {})监听dom变化
更新阶段
beforedestroy
destroyed

6. 组件通信


```js
<template>
  <ul>
   
  </ul>
</template>
{
  data(){
    return {
      age: 10,
      name: 'zs',
      data: {
        size: 10
      }
    }
  },
  created(){
    this.data.size = 20;
    this.$nextTick(() => {

    })
  },
  mounted(){
    this.name = "aaaaa"
  },
  updated(){

  },
  methods: {

  },
  wathch: {
    '$route'(val, oldval){

    },
    'data':{
      deep: true, 
      immediate: true, 
      handler(val, oldval){

      }
    }
  },
  computed: {
    info:{
      get(){
        return '当前用户信息，姓名为：'+ this.name + '年龄为：'+ this.age
      },
      set(){

      }
    }
  }
}
```