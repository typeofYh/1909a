const http = require('http')

const server = http.createServer((req, res) => {
  console.log(req.url);
  if(req.url === '/api/list'){
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000')
    res.end(JSON.stringify({
      data:1,
      msg:'success'
    }))
  }
})

server.listen('3000');
