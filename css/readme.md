### 如何让一个元素水平垂直居中？

[水平垂直居中](./index.html)

1. 给父元素设置`display: flex;`,然后设置`align-items: center;`x轴居中，设置`justify-content: center;`y轴中

2. 通过定位来实现父元素设置`position:relative`,子元素设置`position:absoulte; left:50%; top:50%;`,子元素左上角在中心点，然后通过`transform: translate(-50%, -50%);`让当前元素基于自己本身宽高挪动一半，实现居中

3. 子元素设置宽高的情况下，设置`position: absolute; top:0; left:0; right:0; bottom:0; margin: auto;`

4. 给父元素设置`display: table-cell; vertical-align: middle; text-align: center;`,子元素设置`display: inline-block`

5. `calc()` css计算函数

### bfc 规范， 怎么形成bfc规范？
块级格式化上下文规范， 盒子内的子元素形成自己的布局规范，不会影响外部元素
1. 给父元素设置浮动
2. 给父元素设置overflow:hidden
3. 设置绝对定位

### animation 和 translation 动画
animation动画是关键帧动画，不要事件触发，自动执行
translation动画是过渡动画，需要事件触发时执行


### css盒模型
标准盒子模型：宽度=内容的宽度（content）+ border + padding
低版本IE盒子模型：宽度=内容宽度（content+border+padding)
如何修改盒模型，通过box-sizing:border-box 从标准盒模型编程IE盒模型  box-sizing:content-box 变成标准盒模型

### css选择器优先级
从上到下解析，下面脚本覆盖上面脚本
!important 优先级最高
行内样式
id选择器
class选择器
层级选择器
标签选择器
通配符*

看一下如何替换ui组件库样式，`/deep/` less和sass语法选择器 和 `>>>` css原生选择器 

### 如何隐藏元素
display:none 隐藏元素，不占位隐藏
visibility:hidden 占位隐藏
width:0 height:0 overflow:hidden

### 实现三栏布局，左右固定宽度，中间自适应

<div class="box">
  <div class="left"></div>
  <div class="center"></div>
  <div class="right"></div>
</div>

1. 弹性盒  flex:1 是什么意思？ flex是什么属性的复合属性
2. 定位, 修改父元素的盒模型
3. 浮动

### 圣杯布局