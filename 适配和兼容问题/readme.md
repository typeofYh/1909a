### 移动端适配
 - rem单位：相对单位。相对html的fontSize进行计算
 - rem、em、px的区别 rem和em都是相对单位，rem相对html的fontSize，em相对父元素的fontSize px是绝对单位
 - vh、vw和百分比的区别
 - 怎么实现移动端适配的
   通过rem单位来解决的，根据视口宽度和设计稿宽度计算出来比例，动态修改html的fontSize实现移动端适配，
   基准字体大小为10，方便计算
   开发过程中为了简化px单位转换rem单位计算，引入postcss-pxtorem插件，进行px单位自动转换rem单位，节省开发过程中量出来的px手动计算成rem。
 - 碰见过什么适配问题
   0.5px像素的问题, 通过transfrom: scaleY(0.5); 缩放来实现
   刘海屏 底部导航遮挡 安全区域
   弹窗遮罩 滑动穿透 

### 小程序适配
 - rpx单位

### pc浏览器兼容问题
 - ie和普通浏览器 

### 移动端真机调试
 - 如何真机调试h5页面
  1. 保证手机和电脑在同一个网关下，连同一个网络
  2. windows关闭网络防火墙
  3. 查找电脑ipv4地址， windows通过ipconfig mac通过ifconfig
  4. 打开二维码
 - vconsole 在手机上可以直观看到控制台，方便手机调试问题
 - 抓包工具 线上环境

 pc
 h5 m站 通过手机浏览器访问的
 微信小程序 rpx 
