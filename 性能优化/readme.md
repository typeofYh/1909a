### 前端性能优化

- 代码层面
  react
  vue 

- 静态资源加载优化 html + js + css + 图片

  浏览器缓存
  1. 强缓存（本地缓存命中直接返回资源，不会像服务器发请求）
  服务端返回静态资源的时候设置 
  response headers:
    cache-control: max-age=10(单位是秒)相对时间 /  http2.0
    expires: GMT的时间戳(绝对时间) http1.0
  2. 协商缓存 （服务器得到请求，判断文件是否变化，变化返回200和新资源，没有变化返回304）
  etag: 文件内容的hash映射
  last-modified: 文件最后修改时间 浏览器自动携带 if-modified-since
  浏览器强缓存失效，向服务器发送请求了，然后自动携带If-None-Match 请求头给到服务器
  服务器在一次响应的时候响应etag响应头回去

  200-300之间是成功 200
  300-400之间是重定向  302 临时重定向 304
  400-500之间是错误请求，比如参数错 401 402 403 404
  500以上是服务器错误 

  http传输压缩 gzip
  设置响应头 content-encoding:gzip 然后服务端压缩http传输文件内容

- 打包构建优化 webpack


promise

eventloop

浏览器缓存